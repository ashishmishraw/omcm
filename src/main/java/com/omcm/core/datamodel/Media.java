package com.omcm.core.datamodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MEDIA")
public class Media {
	
    public Media(){}
    
    public Media(String title, String year, String rated, String released, String runtime,
    			String genre, String director, String writer, String actors, String plot,
    			String language, String country, String awards, String poster, String metascore,
    			String imdbrating, String imdbvotes, String imdbid, String type){
    	this.title = title;
    	this.year = year;
    	this.rated = rated;
    	this.released = released;
    	this.runtime = runtime;
    	this.genre = genre;
    	this.director = director;
    	this.writer = writer;
    	this.actors = actors;
    	this.plot = plot;
    	this.language = language;
    	this.country = country;
    	this.awards = awards;
    	this.poster = poster;
    	this.metascore = metascore;
    	this.imdbrating = imdbrating;
    	this.imdbvotes = imdbvotes;
    	this.imdbid = imdbid;
    	this.type = type;
    }
	
    private long id;
	//"Title":
	private String title;
	//"Year":"1999",
	private String year;
	//"Rated":"PG-13",
	private String rated;
	//"Released":"07 May 1999",
	private String released;
	//"Runtime":"125 min",
	private String runtime;
	//"Genre":"Action, Adventure, Fantasy",
	private String genre;
	//"Director":"Stephen Sommers",
	private String director;
	//"Writer":"Stephen Sommers (screen story), Lloyd Fonvielle (screen story), Kevin Jarre (screen story), Stephen Sommers (screenplay)",
	private String writer;
	//"Actors":"Brendan Fraser, Rachel Weisz, John Hannah, Arnold Vosloo",
	private String actors;
	//"Plot":"An American serving in the French Foreign Legion on an archaeological dig at the ancient city of Hamunaptra accidentally awakens a mummy.",
	private String plot;
	//"Language":"English, Egyptian (Ancient), Arabic",
	private String language;
	//"Country":"USA",
	private String country;
	//"Awards":"Nominated for 1 Oscar. Another 5 wins & 19 nominations.",
	private String awards;
	//"Poster":"http://ia.media-imdb.com/images/M/MV5BMTcwMjQ3Mjk0MV5BMl5BanBnXkFtZTgwNzcxNTYxMTE@._V1_SX300.jpg",
	private String poster;
	//"Metascore":"48",
	private String metascore;
	//"imdbRating":"7.0",
	private String imdbrating;
	//"imdbVotes":"315,646",
	private String imdbvotes;
	//"imdbID":"tt0120616",
	private String imdbid;
	//"Type":"movie",
	private String type;
	//"Response":"True"}
	
	@Id
    @GeneratedValue
    @Column(name = "id", unique=true, nullable=false)
    public long getId() {
        return id;
    }
	
	public void setId(long id) {
		this.id = id;
	}
	
	@Column (name="title")
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	@Column (name="year")
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	
	@Column (name="rated")
	public String getRated() {
		return rated;
	}
	public void setRated(String rated) {
		this.rated = rated;
	}
	
	@Column (name="released")
	public String getReleased() {
		return released;
	}
	public void setReleased(String released) {
		this.released = released;
	}
	
	@Column (name="runtime")
	public String getRuntime() {
		return runtime;
	}
	public void setRuntime(String runtime) {
		this.runtime = runtime;
	}
	
	@Column (name="genre")
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	
	@Column (name="director")
	public String getDirector() {
		return director;
	}
	
	public void setDirector(String director) {
		this.director = director;
	}
	
	@Column (name="writer")
	public String getWriter() {
		return writer;
	}
	public void setWriter(String writer) {
		this.writer = writer;
	}
	
	@Column (name="actors")
	public String getActors() {
		return actors;
	}
	
	public void setActors(String actors) {
		this.actors = actors;
	}
	
	@Column (name="plot")
	public String getPlot() {
		return plot;
	}
	
	public void setPlot(String plot) {
		this.plot = plot;
	}
	
	@Column (name="language")
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	
	@Column (name="country")
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	@Column (name="awards")
	public String getAwards() {
		return awards;
	}
	
	public void setAwards(String awards) {
		this.awards = awards;
	}
	
	@Column (name="poster")
	public String getPoster() {
		return poster;
	}
	public void setPoster(String poster) {
		this.poster = poster;
	}
	
	@Column (name="metascore")
	public String getMetascore() {
		return metascore;
	}
	public void setMetascore(String metascore) {
		this.metascore = metascore;
	}
	
	@Column (name="imdbrating")
	public String getImdbrating() {
		return imdbrating;
	}
	
	public void setImdbrating(String imdbrating) {
		this.imdbrating = imdbrating;
	}
	
	@Column (name="imdbvotes")
	public String getImdbvotes() {
		return imdbvotes;
	}
	
	public void setImdbvotes(String imdbvotes) {
		this.imdbvotes = imdbvotes;
	}
	
	@Column(name = "imdbid")
	public String getImdbid() {
		return imdbid;
	}
	public void setImdbid(String imdbid) {
		this.imdbid = imdbid;
	}
	
	@Column (name="type")
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
	public String toString() {
		StringBuffer buf = new StringBuffer();
		return buf.append("{").append(this.id).append(",").append(this.title).append(",").append(this.year).append(",").append(this.rated).append(",")
			.append(this.released).append(",").append(this.runtime).append(",").append(this.genre).append(",").append(this.director)
			.append(",").append(this.writer).append(",").append(this.actors).append(",").append(this.plot).append(",")
			.append(this.language).append(",").append(this.country).append(",").append(this.awards).append(",").append(this.metascore)
			.append(",").append(this.imdbrating).append(",").append(this.imdbvotes).append(",").append(this.imdbid).append(",")
			.append(this.type).append("}").toString();
	}
}
