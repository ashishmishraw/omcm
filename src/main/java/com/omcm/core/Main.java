package com.omcm.core;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.StaticHttpHandler;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.h2.tools.Server;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.omcm.core.datamodel.Media;

import java.io.IOException;
import java.net.URI;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

/**
 * Main class.
 * @author ashishmishraw@bitbucket.org
 */
public class Main {
	
    // Base URI the Grizzly HTTP server will listen on
    public static final String BASE_URI = "http://localhost:8080/omcm/";

    /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     * @return Grizzly HTTP server.
     */
    public static HttpServer startWebServer() {
        // create a resource config that scans for JAX-RS resources and providers in com.omcm.rest package
        final ResourceConfig rc = new ResourceConfig().packages("com.omcm.rest");

        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        HttpServer server =  GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
        
        //add HTTP handler for static web pages in server config 
        server.getServerConfiguration().addHttpHandler(
        		new org.glassfish.grizzly.http.server.CLStaticHttpHandler(Main.class.getClassLoader(), "/webapp/pages/"), "/ui");
      
        return server;
    }
    
    
    /**
     * Starts the database server
     * @return {@link Server} server object
     * @throws SQLException
     */
    public static Server startDatabase() throws SQLException {
    	
    	String args[] = {"-tcpPort", "9092", "-baseDir", ".", "-tcpAllowOthers"};
    	Server server = Server.createTcpServer(args).start();
    	Connection conn = null;
    	try {
    		Class.forName("org.h2.Driver").newInstance();
    		conn = DriverManager.getConnection("jdbc:h2:tcp://localhost/./omcm","sa", "");
    		if (conn == null) return null;
    		//else validateDBSchema();
    	} catch (Exception ex) {
    		ex.printStackTrace();
    	} finally {
    		if ( ! conn.isClosed() ) {
    			conn.close();
    		}
    	}
    	return server;
    }

    /**
     * Main method.
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException, SQLException {
    	
    	final Server dbServer = startDatabase();
    	System.out.println("Starting database ...");

    	
        final HttpServer webServer = startWebServer();
        System.out.println(String.format("Jersey app started with WADL available at "
                + "%sapplication.wadl and UI available at http://localhost:8080/ui/index.html"
                + "\nHit enter to stop it...", BASE_URI));
        
    	validateDBSchema();

        System.in.read();
        webServer.shutdownNow();
        System.out.println("Web server and container down !");
        Server.shutdownTcpServer("tcp://localhost:9092", "", true, true);
        System.out.println("database down !!");
  
    }
    
    
    public static void validateDBSchema() {
    	
    	Media m = new Media("test_movie", "1999", "PG-13", "07 May 1999", "125 min", "Comedy", "Steve",
    				"Kevin Jarre (screen story), Stephen Sommers (screenplay)", "Brendan, Fraser, Rachel, John",
    				"An American serving", "English", "India", "Nominated for 1 Oscar", "http://ia.media-imdb.com/images/M/MV5BMTcwMjQ3Mjk0MV5BMl5BanBnXkFtZTgwNzcxNTYxMTE@._V1_SX300.jpg",
    				"48","7.0","315,646","junk_id","movie");
    	System.out.println("Created sample movie " + m.toString());
    	SessionFactory sessionFactory = new Configuration().configure()
				.buildSessionFactory();
		Session session = sessionFactory.openSession();
		List result = null;
		session.beginTransaction();
		session.save(m);
		
		Media m1 = new Media("desi_movie", "2003", "PG-18", "16 Mar 2003", "125 min", "Adventure", "Dicky",
				"Javis(story), boobie (screenplay)", "Julian, Rakette, Mohima",
				"An Indian saga", "Hindi", "India", "Nominated for 1 Filmfare", "http://ia.media-imdb.com/images/M/MV5BMTcwMjQ3Mjk0MV5BMl5BanBnXkFtZTgwNzcxNTYxMTE@._V1_SX300.jpg",
				"53","5.1","1,000","desi_id","movie");
		
		session.save(m1);
		
		result = session.createQuery("from Media").list();
		for (Media movie : (List<Media>) result) {
			System.out.println(movie.getTitle());
		}
		session.delete(m); session.delete(m1);
		
		result = session.createQuery("from Media").list();
		if ( ((List<Media>) result).size() == 0 )
			System.out.println("Cleaned up Media table ");
		
		session.getTransaction().commit();
		session.close();
    }
}

