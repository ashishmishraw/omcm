package com.omcm.core.db;

import java.util.List;

import org.dom4j.tree.AbstractEntity;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.omcm.core.datamodel.Media;

/**
 * Utility class - provides ORM utilities for accessing underlying database
 *  @author ashishmishraw@bitbucket.org
 * @since PoC
 */
public class ORMUtil {
	
	private static final SessionFactory sessionFactory;
	private static Session currentSession;
	
	static {
		try {
			sessionFactory = new Configuration().configure()
					.buildSessionFactory();
		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	/**
	 * Singleton for obtaining {@link SessionFactory} sessionFactory
	 */
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
	
	
	/**
     * Handle sessions and close them at end of HTTP transaction instead of
     * after tx.commit()
     * 
     * @return the session
     */
    public static Session getSession() {
		if (currentSession == null) {
		    currentSession = sessionFactory.openSession();
		}
		return currentSession;
    }

    /**
     * Close open session.
     */
    public static void closeSession() throws HibernateException {
		if (currentSession != null) {
		    currentSession.close();
		    currentSession = null;
		}
    }
    
    /**
     * Saves the provided entity object (mapped relationally) into database 
     * @param entity
     */
    public static void save(Media media) throws HibernateException {
    	
    	Session session = getSession();
    	Transaction tx = session.beginTransaction();
    	session.evict(media);
    	session.saveOrUpdate(media);
    	tx.commit();
    }
    
    
    /**
     * Util method to find if an entity exists in database by criteria mentioned in where clause
     * @param clazz
     * @param whereClause
     */
    public static boolean ifExists(Class clazz, String whereClause ) {
    	
    	String whereHQL = "";
    	if (whereClause != null && whereClause.trim().length() > 0)
    	    whereHQL = " where " + whereClause;
    	
    	Session session = getSession();
    	Transaction tx = session.beginTransaction();
    	
    	List result = session.createQuery("from " + clazz.getSimpleName() + whereHQL).list();
    	
    	tx.commit();
    	
    	if(result.size() ==1) {
    		return true;
    	}
    	return false;
    }
    
    
	@SuppressWarnings("unchecked")
    public static List<Media> orderedList(String orderByCriteria) {
    	
    	String orderByCLause = "";
    	if ( null != orderByCriteria && orderByCriteria.trim().length() > 0)
    		orderByCLause = " order by " + orderByCriteria;
    	
    	Session session = getSession();
    	Transaction tx = session.beginTransaction();	
    
		List<Media> result = session.createQuery("from Media" + orderByCLause).list();
    	tx.commit();
    	
    	return result;
    }

}
