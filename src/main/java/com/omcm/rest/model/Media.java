package com.omcm.rest.model;

/**
 * Abstract class media
 * @author ashismis
 *
 */
public abstract class Media {
	
	private long id;
	private String title;
	private String yop;//year of production
	private String director;
	private String genre;
	private String rating;
	private String excerpts;
	private String resolution;
	
	
	public Media(String title, String yop, String director, String genre, 
			String rating, String excerpts, String resolution) {
		this.title = title;
		this.yop = yop;
		this.director = director;
		this.genre = genre;
		this.rating = rating;
		this.excerpts = excerpts;
		this.resolution = resolution;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getYop() {
		return yop;
	}
	public void setYop(String yop) {
		this.yop = yop;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public String getExcerpts() {
		return excerpts;
	}
	public void setExcerpts(String excerpts) {
		this.excerpts = excerpts;
	}
	public String getResolution() {
		return resolution;
	}
	public void setResolution(String resolution) {
		this.resolution = resolution;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
}
