package com.omcm.rest.model;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * MediaContentList class
 * @author ashishmishraw@bitbucket.org
 *
 */
public class MediaContentList {
	
	private static final CopyOnWriteArrayList<Movie> movieList = new CopyOnWriteArrayList<>();
	//private static final CopyOnWriteArrayList<Customer> tvshowList = new CopyOnWriteArrayList<>();
	//private static final CopyOnWriteArrayList<Customer> musicList = new CopyOnWriteArrayList<>();
	
	private MediaContentList(){}
	
	public static CopyOnWriteArrayList<Movie> getInstance() {
		return movieList;
	}
	
	public boolean addMovie( Media movie ) {
		movieList.add((Movie) movie);
		return true;
	}
	
	
	static {
		Movie m = new Movie("Mission Impossible", "1992", "Mad guy", "action", "6.7", "its really possible", "HD 720p");
		m.setId(627022936);
		movieList.add(m);
		movieList.add(new Movie("John Tucker", "2006", "REfflemen", "drama", "5.0", "Bad guy gets lesson", "SD 480p"));
		movieList.add(new Movie("Jaws", "1972", "Michael", "thriller", "7.6", "sharks coming ..", "HD 1080p"));
	}
}
