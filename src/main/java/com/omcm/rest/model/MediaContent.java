package com.omcm.rest.model;

/**
 * MediaContent interface
 * @author ashishmishraw@bitbucket.org
 *
 */
public interface MediaContent {

	public long getId();
	
}
