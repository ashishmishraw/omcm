package com.omcm.rest.model;

/**
 * Movie base model class
 * @author ashishmishraw@bitbucket.org
 * @since PoC
 */
public class Movie extends Media implements MediaContent {

	@Override
	public long getId() {
		if (0 == super.getId())
			return this.hashCode();
		else return super.getId();
	}
	
	/**
	 * Constructor with title
	 * @param title
	 */
	public Movie(String title) {
		super(title, null, null, null, null, null, null);
	}
	
	/**
	 * Constructor with all params
	 * @param title
	 * @param yop
	 * @param director
	 * @param genre
	 * @param rating
	 * @param excerpts
	 * @param resolution
	 */
	public Movie(String title, String yop, String director, String genre, 
			String rating, String excerpts, String resolution) {
		super(title, yop, director, genre, rating, excerpts, resolution);
	}
	
	
	@Override
	public String toString() {
		return "{ title: " + this.getTitle() + ", yop: " + this.getYop() + ", director: " + 
				this.getDirector() + ", genre: " + this.getGenre() + ", rating: " + this.getRating()
				+ ", excerpts: " + this.getExcerpts() + ", resolution: " + this.getResolution();
	}

}
