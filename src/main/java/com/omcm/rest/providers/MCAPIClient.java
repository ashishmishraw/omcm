package com.omcm.rest.providers;



import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;
import javax.ws.rs.core.HttpHeaders;

import org.eclipse.persistence.oxm.MediaType;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.RequestBuilder;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.client.urlconnection.HTTPSProperties;


/**
 * Media Content API client class
 * Fetches media content info objects from various content providers
 * 
 * Generic to work with any content provider 
 * 
 * @author ashishmishraw@bitbucket.org
 * @since PoC
 */
public class MCAPIClient {

	private String apiHost= "www.omdbapi.com";
	private static Client client;
	
	private static final int MCAPI_HTTP_CONNECTION_TIMEOUT= 5000; //TODO - props based
	private static final int MCAPI_HTTP_READ_TIMEOUT=5000; //TODO - props based
	
	static {
		ClientConfig cfg = TrustManager.acceptAllCertificates();
		client = Client.create(cfg);
		
		client.setConnectTimeout(MCAPI_HTTP_CONNECTION_TIMEOUT);
		client.setReadTimeout(MCAPI_HTTP_READ_TIMEOUT); 
	}
	
	
	/**
	 * APi for executing HTTP GET request on the Media Content Provider API based on operation type
	 * @param opType Operation type
	 * @return {@link ClientResponse} response
	 */
	public ClientResponse get(MCAPIUtil.ACTION opType, String id) throws UnsupportedEncodingException {
		
		String url = MCAPIUtil.getProviderURL(apiHost, opType);
		id = URLEncoder.encode(id,"UTF-8");
		url = url.concat(id);
		WebResource webResource = client.resource(url);
		ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);
		
		return response;
	}
	
	
	
	/**
	 * Static inner class - trust manager
	 *
	 */
	static class TrustManager implements X509TrustManager{
		
		@Override
		public X509Certificate[] getAcceptedIssuers() { 
			return new X509Certificate[0];
		}

		@Override
		public void checkServerTrusted(X509Certificate[] arg0, String arg1) 
				throws CertificateException { }

		@Override
		public void checkClientTrusted(X509Certificate[] arg0, String arg1)
				throws CertificateException { }
		
		
		public static ClientConfig acceptAllCertificates() {
			 ClientConfig cfg = new DefaultClientConfig();

				try {
					SSLContext sslContext = SSLContext.getInstance("SSL");
					// trust all certificates
					sslContext.init(null, new TrustManager[]{ new TrustManager()},new  SecureRandom());
					cfg.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, new HTTPSProperties(
							new HostnameVerifier() {
								@Override
								public boolean verify(String hostname, SSLSession session) {
									return true;
								}
							},sslContext ));
				} catch (NoSuchAlgorithmException e) {
					e.getStackTrace();
				} catch (KeyManagementException e) {
					e.printStackTrace();
				}
				return cfg;
		}
	}
}
