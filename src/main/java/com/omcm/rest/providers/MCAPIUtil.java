package com.omcm.rest.providers;

/**
 * Utility class to get particular API URL based on content provider type
 * @author ashishmishraw@bitbucket.org
 * @since PoC
 */
public class MCAPIUtil {
	
	/**
	 * Gets the URL based on API host and type
	 * @param apiHost
	 * @return String URL
	 */
	public static String getProviderURL(String apiHost, ACTION opType) {
		
		StringBuffer pURL = 
				new StringBuffer("https://").append(apiHost).append("/");
		
		switch(opType) {
		case SEARCH :
			pURL.append("?s=");
			break;
		case INFO:
			pURL.append("?t=");
			break;
		default: break;
		}
		
		return pURL.toString();
	}
	
	
	
	public enum ACTION {
		
		SEARCH("SEARCH"), INFO("INFO");
		
		private final String type ;
		
		private ACTION(String type) {
			this.type = type;
		}
		
		public String getType() {
			return type;
		}
		
		@Override
		public String toString() {
			return getType();
		}
	}

}
