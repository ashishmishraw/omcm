package com.omcm.rest.providers;

import java.io.UnsupportedEncodingException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.omcm.core.datamodel.Media;
import com.omcm.rest.providers.MCAPIUtil.ACTION;
import com.sun.jersey.api.client.ClientResponse;

/**
 * Implementing class for MediaContentProvider
 * @author ashishmishraw@bitbucket.org
 *
 */
public class MediaContentProviderImpl implements MediaContentProvider {
	
	
	/**
	 * Returns exact media object which matches with title provided
	 */
	@Override
	public Media getMediaContentInfo(String title) throws UnsupportedEncodingException{
		
		MCAPIClient provider = new MCAPIClient();
		ClientResponse response = provider.get(ACTION.INFO, title);
		String json =response.getEntity(String.class);
		
		if (json.contains(",\"Response\":\"True\"}") ){
			json = json.substring(0, json.indexOf(",\"Response\":\"True\""));
			System.out.println("" + json); 
		}
		json = json + "}";
		Media media  = null;
		try {
			media = new ObjectMapper().readValue(json.toLowerCase(), Media.class);	
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return media;
	}

	/**
	 * Searches Media content, returns list of media matches with title
	 */
	@Override
	public String searchMediaContent(String title) throws UnsupportedEncodingException{
		MCAPIClient provider = new MCAPIClient();
		ClientResponse response = provider.get(ACTION.SEARCH,title);
		String json = response.getEntity(String.class);
		return json;
	}

}
