package com.omcm.rest.providers;

import java.io.UnsupportedEncodingException;

import com.omcm.core.datamodel.Media;

/**
 * interface Media content provider
 * @author ashishmishraw@bitbucket.org
 *
 */
public interface MediaContentProvider {
	
	public Media getMediaContentInfo(String id) throws UnsupportedEncodingException;
	
	public String searchMediaContent(String title) throws UnsupportedEncodingException;

}
