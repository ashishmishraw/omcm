package com.omcm.rest.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Root resource (exposed at "myresource" path)
 *  @author ashishmishraw@bitbucket.org
 *  
 */
@Path("mediaresource")
public class MediaResource {

    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
    @Produces({MediaType.TEXT_PLAIN , MediaType.APPLICATION_JSON})
    public String getIt() {
        return "Media content management !";
    }
}