package com.omcm.rest.api;

import javax.ws.rs.core.Response;

/**
 * Base class
 * @author ashishmishraw@bitbucket.org
 *
 */
public class MediaServiceBase {
	
	/**
	 * 
	 * @return
	 */
	 public Response validate() {
		 return null;
	 }
	 
	 /**
	  * 
	  * @param ex
	  * @return
	  */
	 public Response generateErrorResponse(Throwable ex) {
		 Response.ResponseBuilder b = Response.status(Response.Status.BAD_REQUEST);
		 return b.build();
	 }

}
