package com.omcm.rest.api;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.HibernateException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.omcm.core.datamodel.Media;
import com.omcm.core.db.ORMUtil;
import com.omcm.rest.model.MediaContentList;
import com.omcm.rest.model.Movie;
import com.omcm.rest.providers.MediaContentProvider;
import com.omcm.rest.providers.MediaContentProviderImpl;

/**
 * Movie content manager
 * @author ashishmishraw@bitbucket.org
 *
 */
@Path("/movies")
public class MovieContentManager extends MediaServiceBase {
	private final CopyOnWriteArrayList<Movie> movieList = MediaContentList.getInstance();
	private ObjectMapper mapper = new ObjectMapper();

	/**
	 * Gets all movies from collection 
	 * @return String JSON response
	 */
	@GET
	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAllMovies() {
		/*
		try {
			return mapper.writeValueAsString(movieList);
		} catch (JsonProcessingException jsonex) {
			jsonex.getStackTrace();
			return "---Customer List---\n"
			+ movieList.stream()
			.map(c -> c.toString())
			.collect(Collectors.joining("\n"));
		}	*/
		List<Media> allMedia = null;
		String jsonResponse = null;
		try {
			allMedia = ORMUtil.orderedList("title");
			
			if (null != allMedia && allMedia.size() > 0) {
				jsonResponse = new ObjectMapper().writeValueAsString(allMedia);
			}
			
		} catch (HibernateException hex) {
			hex.printStackTrace();
			return "{Error:Error while getting data}";
		} catch (Exception ex) {
			ex.printStackTrace();
			return "{Error:Error}";
		}
		
		if( jsonResponse != null) return jsonResponse;
		else return "{}";
	}
	  
	/**
	 * Get a specific movie by Id
	 * @param id - movie id
	 * @return {@link Movie} object as JSON string
	 */
	
	@GET
	@Path("/id/{id}")
	@Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
	public String getMovieById(@PathParam("id") long id) {
		
		Optional<Movie> match = movieList.stream()
			.filter(c -> c.getId() == id)
	        .findFirst();
	    if (match.isPresent()) {
	      try {
	    	  return mapper.writeValueAsString(match.get());
	      } catch (JsonProcessingException jsonex) {
			  jsonex.getStackTrace();
			  return "---Movie---\n" + match.get().toString();
	      }
	    } else {
	      return "Movie not found";
	    }
	}
	
	
	/**
	 * Get single movie by title
	 * TODO - may return multiple titles also, handle that
	 * @param title of movie
	 * @return {@link Movie} object as JSON string
	 */
	@GET
	@Path("/title/{title}")
	@Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
	public String getMovieByTitle(@PathParam("title") String title) throws UnsupportedEncodingException {
		
		title = URLDecoder.decode(title,"UTF-8");
		
		for (Movie m : movieList) {
			if (m.getTitle().equalsIgnoreCase(title))
				return m.toString();
		}
	    return "Movie not found";
	}
	
	
	@GET
	@Path("/search/{title}")
	@Produces({MediaType.APPLICATION_JSON})
	public String searchMovie(@PathParam("title") String title) throws UnsupportedEncodingException {
		
		title = URLDecoder.decode(title,"UTF-8");	
		MediaContentProvider provider = new MediaContentProviderImpl();
		return provider.searchMediaContent(title);
	}
	
	
	/**
	 * Add a movie to collection
	 * @param title of movie
	 * @return status
	 */
	@GET
	@Path("/add/{title}")
	@Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON}) 
	public String addMovie(@PathParam("title") String title ) {
		
		MediaContentProvider provider = new MediaContentProviderImpl();
		//return provider.getMediaContentInfo(title).toString();
		Media media = null;
		try { 
			title = URLDecoder.decode(title,"UTF-8");
			media = provider.getMediaContentInfo(title);
			if ( null != media) {
				ORMUtil.save(media);
				ORMUtil.closeSession();
			}
		} catch (UnsupportedEncodingException uex) {
			System.err.println("Exception while encoding title of media " + title );
			uex.printStackTrace();
			return Response.Status.BAD_REQUEST.toString();
		} catch (HibernateException hex) {
			System.err.println("Exception while performing db operation " + hex.getMessage());
			hex.printStackTrace();
			return Response.Status.INTERNAL_SERVER_ERROR.toString();
		} catch (Exception ex) {
			System.err.println("Error performing add operation for " + title + " : cause -> " + ex.getMessage() );
			ex.printStackTrace();
			return Response.Status.INTERNAL_SERVER_ERROR.toString();
		}
		return Response.Status.CREATED.toString();
	}
	
	
	/*
	 * TODO - make it work via reflection so that we could find by any fields / criteria
	 */
	private Optional<Movie> findByCriteria(String criteria, String value) {
		
		Movie m =  new Movie(value);
		Optional<Movie> match = null;
		try {
			Field field = m.getClass().getDeclaredField(criteria);
			Object v =  field.get(m);
			match = movieList.stream().filter(c -> v == value).findFirst();
		} catch (Exception ne) {
			ne.printStackTrace();
		}
		m = null;
		if ( match.isPresent() ) {
			return match;
		}
		return null;
	}
	
}
